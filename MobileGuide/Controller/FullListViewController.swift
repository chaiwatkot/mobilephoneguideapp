//
//  FullListViewController.swift
//  MobileGuide
//
//  Created by Chaiwat Chanthasen on 27/8/2562 BE.
//  Copyright © 2562 SCB. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import XLPagerTabStrip

class FullListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
  var mDataFeed: FeedData?
  var mDataResponse: StructureResponse?
  var mShareVar = ServiceManager.shared
  var mMobileID: Int?
  var userDefault = UserDefaults.standard
  var idUserFavourite: [Int]?
  var mImageStar: UIImage?
  @IBOutlet weak var mTableView: UITableView?
  @IBOutlet weak var mSortButton: UIBarButtonItem?
  @IBOutlet weak var mSegment: UISegmentedControl?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.mDataFeed = FeedData()
    self.feedData()

//    userDefault.removeObject(forKey: "data")
    print(UserDefaults.standard.object(forKey: "data") as Any)
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return mDataResponse?.count ?? 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "scbcell") as? CustomTableViewCell else {
      return UITableViewCell()
    }
    let item = mDataResponse?[indexPath.row]
    let view = UIView(frame: cell.frame)
    view.backgroundColor = UIColor.red.withAlphaComponent(0.3)
    cell.selectedBackgroundView = view
    cell.setUpButtun(tap: item?.isFav ?? false)
    cell.changeButtonImage(isTap: item?.isFav ?? false)
    if item?.isFav ?? false {
    updateDataSort(indexIsFavourite: indexPath.row)
    }
    cell.fullListViewController = self
    cell.mTitle?.text = item?.name
    cell.mDescription?.text = item?.description
    cell.mPrice?.text = "Price: \(item?.price ?? 0)"
    cell.mRating?.text = "Rating: \(item?.rating ?? 0)"
    cell.mImageView?.loadImageUrl(item?.thumbImageURL ?? "")
    print(item?.isFav)
    
    return cell
  }
  
  private func setUserDefaultData(){
    mShareVar.mDataFavouriteDic.removeAll()
    mShareVar.mMobileIDData.removeAll()
    idUserFavourite = UserDefaults.standard.object(forKey: "data") as? [Int] ?? []
    for index in idUserFavourite ?? []{
      mShareVar.mDataFavouriteDic.updateValue(self.mDataResponse![index-1], forKey: index)
      if let userFavourite = mDataResponse?[index-1].isFav {
        mDataResponse?[index-1].isFav = !userFavourite
      }
      print(mShareVar.mDataFavouriteDic)
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "showDetail",
      let viewController = segue.destination as? DetailViewController,
      let selectedTrack = sender as? PurpleStructureResponse {
      viewController.mDataMobile = selectedTrack
    }
  }
  
  func addCellToFavourite(cell: UITableViewCell, tap: Bool) {
    guard var mDataResponse1 = self.mDataResponse else { return }
    let indexPathTap = mTableView?.indexPath(for: cell)
    let index = indexPathTap?.row ?? 0
    self.mDataResponse?[index].isFav = tap
    mShareVar.mMobileID = self.mDataResponse?[index].id
    mShareVar.mDataFavouriteDic.updateValue((self.mDataResponse?[index])!, forKey: self.mShareVar.mMobileID)
    mShareVar.mMobileIDData.removeAll()
    print(mShareVar.mDataFavouriteDic.count)
    mTableView?.reloadData()
  }
  
  func deleteCellFavourite(cell: UITableViewCell, tap: Bool){
    guard var mDataResponse1 = self.mDataResponse else { return }
    let indexPathTap = mTableView?.indexPath(for: cell)
    let index = indexPathTap?.row ?? 0
    mDataResponse?[index ].isFav = tap
    mShareVar.mMobileID = self.mDataResponse![index].id
    mShareVar.mDataFavouriteDic.removeValue(forKey: self.mShareVar.mMobileID)
    mShareVar.mMobileIDData.removeAll()
//    let indexArray = self.mShareVar.mMobileIDData.firstIndex(of: self.mDataResponse![index].id) ?? 0
//    mShareVar.mMobileIDData.remove(at: indexArray)
//    print(mShareVar.mMobileIDData)
//    print(mShareVar.mMobileID)
    print(mShareVar.mDataFavouriteDic.count)
    mTableView?.reloadData()
  }
  
  func updateDataSort(indexIsFavourite: Int){
      mShareVar.mMobileIDData.append((self.mDataResponse?[indexIsFavourite].id)!)
      print("\(mShareVar.mMobileIDData)++++++")
    saveUserDefault()
  }
  
  func saveUserDefault() {
    userDefault = UserDefaults.standard
    userDefault.set(mShareVar.mMobileIDData, forKey: "data")
    userDefault.synchronize()
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    print("++++++++\(self.mDataResponse![indexPath.row])+++++++++")
    self.performSegue(withIdentifier: "showDetail", sender: self.mDataResponse?[indexPath.row])
  }
  
  private func feedData(){
    let url = "https://scb-test-mobile.herokuapp.com/api/mobiles/"
    self.mDataFeed?.getMobileData(url: url) { (result) in
      self.mDataResponse = result
      self.setUserDefaultData()
      self.mTableView?.reloadData()
    }
  }
  
  @IBAction func showSortAlert(){
    self.showAlertWithThreeButton()
  }
  
  private func showAlertWithThreeButton() {
    let alert = UIAlertController(title: "Sort", message: nil, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Price low to high", style: .default, handler: { (_) in
      self.mDataResponse?.sort(by: { (first, second) -> Bool in
        first.price < second.price
      })
      self.mShareVar.mMobileIDData.removeAll()
      self.mTableView?.reloadData()
    }))
    
    alert.addAction(UIAlertAction(title: "Price high to low", style: .default, handler: { (_) in
      self.mDataResponse?.sort(by: { (first, second) -> Bool in
        first.price > second.price
      })
      self.mShareVar.mMobileIDData.removeAll()
      self.mTableView?.reloadData()
    }))
    
    alert.addAction(UIAlertAction(title: "Rating", style: .default, handler: { (_) in
      self.mDataResponse?.sort(by: { (first, second) -> Bool in
        first.rating > second.rating
      })
      self.mShareVar.mMobileIDData.removeAll()
      self.mTableView?.reloadData()
    }))
    alert.addAction(UIAlertAction(title: "Cancle", style: .cancel, handler: nil))
    self.present(alert, animated: true, completion: nil)
  }
  
  @IBAction func changeLang(_ sender: AnyObject) {
    if sender.selectedSegmentIndex == 0 {
    } else {
      performSegue(withIdentifier: "showFavourite", sender: nil)
    }
  }
}

