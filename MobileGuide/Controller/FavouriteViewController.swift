//
//  FavouriteViewController.swift
//  MobileGuide
//
//  Created by Chaiwat Chanthasen on 27/8/2562 BE.
//  Copyright © 2562 SCB. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import XLPagerTabStrip

class FavouriteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
  
  var mDataMobile: [PurpleStructureResponse] = []
  var mShareVar = ServiceManager.shared
  @IBOutlet weak var mTableView: UITableView?
  @IBOutlet weak var mSortButton: UIBarButtonItem?
  var userDefaultData: String?
  var favouriteID: [Int]?
  var mItem: PurpleStructureResponse?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
  }
  
  override func viewWillAppear(_ animated: Bool) {
    getItemWithID()
    mTableView?.reloadData()
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return mDataMobile.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "favouriteCell") as? FavouriteTableViewCell else {
      return UITableViewCell()
    }
    let item = mDataMobile[indexPath.row]
    let view = UIView(frame: cell.frame)
    view.backgroundColor = UIColor.red.withAlphaComponent(0.3)
    cell.selectedBackgroundView = view
    cell.mTitle?.text = item.name
    cell.mDescription?.text = item.description
    cell.mPrice?.text = "Price: \(String(describing: item.price) )"
    cell.mRating?.text = "Rating: \(String(describing: item.rating) )"
    cell.mImageView.loadImageUrl(item.thumbImageURL )

    return cell
  }
  
  func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
    return .delete
  }

  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    if (editingStyle == UITableViewCell.EditingStyle.delete) {
      mShareVar.mDataFavouriteDic.removeValue(forKey: mDataMobile[indexPath.row].id)
      mTableView?.beginUpdates()
      mTableView?.deleteRows(at: [indexPath], with: .fade)
      let indexArray = self.mShareVar.mMobileIDData.firstIndex(of: mDataMobile[indexPath.row].id) ?? 0
      mShareVar.mMobileIDData.remove(at: indexArray)
      mDataMobile.remove(at: indexPath.row)
     
      print(UserDefaults.standard.object(forKey: "data") as Any)
      mTableView?.reloadData()
      mTableView?.endUpdates()
    }
  }
  
  func saveUserDefault() {
    let userDefault = UserDefaults.standard
    userDefault.set(mShareVar.mMobileIDData, forKey: "data")
    userDefault.synchronize()
  }

  func getItemWithID(){
    mDataMobile.removeAll()
    favouriteID = UserDefaults.standard.object(forKey: "data") as? [Int] ?? []
    for index in favouriteID ?? [] {
      if let dataFav = mShareVar.mDataFavouriteDic[index] {
        mDataMobile.append(dataFav) }
    }
    print("\(mDataMobile)=================")
    mTableView?.reloadData()
  }
  
  @IBAction func changeLang(_ sender: AnyObject) {
    if sender.selectedSegmentIndex == 0 {
      mShareVar.mMobileIDData.removeAll()

      performSegue(withIdentifier: "showFullList", sender: nil)
    } else {
    }
  }
}




/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */


