//
//  DetailViewController.swift
//  MobileGuide
//
//  Created by Chaiwat Chanthasen on 27/8/2562 BE.
//  Copyright © 2562 SCB. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class DetailViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
  
  var mDataFeed:FeedData!
  var mDataResponse:TopLevel!
  var mDataMobile:PurpleStructureResponse!
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  //    @IBOutlet weak var mTitle:UILabel!
  @IBOutlet weak var mDescription:UILabel!
  @IBOutlet weak var mPrice:UILabel!
  @IBOutlet weak var mRating:UILabel!
  @IBOutlet weak var mTitleNavigation:UINavigationItem!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.mDataFeed = FeedData()
    self.getMobileByID()
    self.setUI()
    collectionView.delegate = self
    collectionView.dataSource = self
    // Do any additional setup after loading the view.
  }
  
  func getMobileByID() {
    let id = mDataMobile.id
    let url = "https://scb-test-mobile.herokuapp.com/api/mobiles/\(id)/images/"
    self.mDataFeed.getMobileDataByID(url: url) { (result) in
      self.mDataResponse = (result as! TopLevel)
      self.collectionView.reloadData()
      print(self.mDataResponse.count)
      print(self.mDataResponse!)
    }
  }
  
  func setUI(){
    mRating.text = "Rating: \(self.mDataMobile.rating)"
    mPrice.text = "Price: \(self.mDataMobile.price)"
    mDescription.text = self.mDataMobile.description
    mTitleNavigation.title = mDataMobile.name
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if let count = self.mDataResponse?.count{
      return count
    }else{
      return 0
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCollectionCell", for: indexPath) as! ImageCollectionCell
    var item = self.mDataResponse[indexPath.row].url
    item = self.checkHTTP(url: item)
    //        print("\(item)==================================================")
    cell.mImageMobile.loadImageUrl(item)
    return cell
  }
  
  func isValidHTTP(url:String) -> Bool{
    let head1 = "((http|https)://)"
    let head = "([(w|W)]{3}+\\.)?"
    let tail = "\\.+[A-Za-z]{2,3}+(\\.)?+(/(.)*)?"
    let urlRegEx = head1 + head + "+(.)+" + tail
    let httpTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
    return httpTest.evaluate(with: url)
  }
  
  func checkHTTP(url:String) -> String{
    var link:String = url
    if isValidHTTP(url: url){
      //            print("correct")
    }else{
      link = "https://\(url)"
    }
    return link
  }
}
