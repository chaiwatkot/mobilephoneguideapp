
import Foundation
import UIKit
import Alamofire

class FavouriteTableViewCell:UITableViewCell{
  
  @IBOutlet weak var mTitle:UILabel?
  @IBOutlet weak var mDescription:UILabel?
  @IBOutlet weak var mPrice:UILabel?
  @IBOutlet weak var mRating:UILabel?
  
  @IBOutlet weak var mImageView:UIImageView!
  
}
