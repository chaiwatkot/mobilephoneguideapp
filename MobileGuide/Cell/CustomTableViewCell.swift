//
//  CustomTableView.swift
//  MobileGuide
//
//  Created by Chaiwat Chanthasen on 27/8/2562 BE.
//  Copyright © 2562 SCB. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class CustomTableViewCell:UITableViewCell{
  
  @IBOutlet weak var mTitle: UILabel?
  @IBOutlet weak var mDescription: UILabel?
  @IBOutlet weak var mPrice: UILabel?
  @IBOutlet weak var mRating: UILabel?
  @IBOutlet weak var mImageView: UIImageView?
  @IBOutlet weak var mButton: UIButton?
  var mImageStar: UIImage?
  var mWhenTap: Bool = false
  var fullListViewController: FullListViewController?
  var mShareVar = ServiceManager.shared
  
  func setUpButtun(tap: Bool) {
    mButton?.tintColor = UIColor.blue
    self.mWhenTap = tap
  }
  
  @IBAction func handleMarkFavorite() {
    if mWhenTap {
      self.mWhenTap = false
      fullListViewController?.deleteCellFavourite(cell: self, tap: mWhenTap)
    } else {
      self.mWhenTap = true
      fullListViewController?.addCellToFavourite(cell: self, tap: mWhenTap)
    }
  }
  
  func changeButtonImage(isTap: Bool) {
    if isTap {
      mImageStar = UIImage(named: "star2.png")!
    } else {
      mImageStar = UIImage(named: "star1.png")!
    }
    mButton?.setImage(mImageStar, for: .normal)
  }
}
