//
//  FeedData.swift
//  MobileGuide
//
//  Created by Chaiwat Chanthasen on 27/8/2562 BE.
//  Copyright © 2562 SCB. All rights reserved.
//

import Foundation
import Alamofire

class FeedData{
  
  func getMobileData(url: String, completion: @escaping (StructureResponse) -> Void){
    AF.request(URL(string: url)!, method: .get).responseJSON { response in
      switch response.result {
      case .success:
       
        do {
          let decoder = JSONDecoder()
          let result = try decoder.decode(StructureResponse.self, from: response.data!)
          completion(result)
        } catch let error{
          print(error)
        }
      case let .failure(error):
        print(error)
      }
    }
  }
  
  func getMobileDataByID(url: String, completion: @escaping (Any) -> Void){
    AF.request(URL(string: url)!, method: .get).responseJSON { response in
      switch response.result {
      case let .success(value):
        print(value)
        do {
          let decoder = JSONDecoder()
          let result = try decoder.decode(TopLevel.self, from: response.data!)
          completion(result)
        } catch let error{
          print(error)
        }
      case let .failure(error):
        print(error)
      }
    }
  }
  
  
  
}
