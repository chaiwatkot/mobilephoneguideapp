//
//  StructureResponse.swift
//  MobileGuide
//
//  Created by Chaiwat Chanthasen on 27/8/2562 BE.
//  Copyright © 2562 SCB. All rights reserved.
//

import Foundation

typealias StructureResponse = [PurpleStructureResponse]

struct PurpleStructureResponse: Codable {
  let thumbImageURL, brand: String
  let rating: Double
  let id: Int
  let name, description: String
  let price: Double
  var isFav: Bool = false
  
  enum CodingKeys: String, CodingKey {
    case thumbImageURL, brand, name, description, id, rating, price
  }
}

typealias TopLevel = [PurpleTopLevel]

struct PurpleTopLevel: Codable {
  let mobileID: Int
  let url: String
  let id: Int
  
  enum CodingKeys: String, CodingKey {
    case mobileID = "mobile_id"
    case url, id
  }
}

