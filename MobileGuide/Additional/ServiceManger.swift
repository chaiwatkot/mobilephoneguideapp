//
//  ServiceManager.swift
//  Appointments
//
//  Created by Preeyapol Owatsuwan on 2/8/2562 BE.
//  Copyright © 2562 my. All rights reserved.
//

import UIKit
import Alamofire

class ServiceManager {
  static let shared = ServiceManager()
  
  //shared
  var mDataFavorite = [PurpleStructureResponse]()
  var mDataResponseShared:StructureResponse!
  var mMobileID: Int!
  var mDataFavouriteDic: [Int:PurpleStructureResponse] = [:]
  var mMobileIDData:[Int] = []
}
