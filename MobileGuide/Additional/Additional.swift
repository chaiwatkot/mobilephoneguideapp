//
//  Additional.swift
//  MobileGuide
//
//  Created by Chaiwat Chanthasen on 27/8/2562 BE.
//  Copyright © 2562 SCB. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView{
  func loadImageUrl(_ urlString:String){
    self.af_setImage(withURL: URL(string: urlString)!)
  }
  
}
